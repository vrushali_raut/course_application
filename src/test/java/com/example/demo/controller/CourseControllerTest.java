package com.example.demo.controller;

import com.example.demo.entities.Course;
import com.example.demo.service.CourseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseControllerTest {

    @InjectMocks
    CourseController courseController;

    @Mock
    CourseService courseService;

    @Test
    public void getCourses() {
        List<Course> list = expectedMockList();
        when(courseService.getCourses()).thenReturn(list);
        List<Course> courses = courseController.getCourses();
        assertEquals(list, courses);
    }

    private List<Course> expectedMockList() {
        List<Course> list = new ArrayList<>();
        list.add(new Course(1, "Java course", "This course contain basics of springboot"));
        list.add(new Course(2, "Golang course", "This course contain basics of golang gin framework"));
        return null;
    }

    @Test
    public void getCourse() {
    }
}